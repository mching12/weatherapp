package myapps.com.weatherapp.weatherdetail;

import android.databinding.DataBindingUtil;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;

import com.squareup.picasso.Picasso;

import myapps.com.weatherapp.BuildConfig;
import myapps.com.weatherapp.R;
import myapps.com.weatherapp.RefreshFragment;
import myapps.com.weatherapp.Utils;
import myapps.com.weatherapp.data.model.WeatherData;
import myapps.com.weatherapp.databinding.ActivityWeatherDetailBinding;
import myapps.com.weatherapp.main.WeatherApp;

public class WeatherDetailActivity extends AppCompatActivity implements RefreshFragment.RefreshListener, WeatherDetailContract.View {

    public static final String DATA_EXTRA = "data_extra";

    private ActivityWeatherDetailBinding binding;
    private WeatherDetailContract.Presenter presenter;
    private RefreshFragment refreshFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_weather_detail);

        initialize();
    }

    private void initialize() {
        refreshFragment = RefreshFragment.getRefreshFragment(this);
        setFragment(refreshFragment, binding.refresh);
        presenter = new WeatherDetailPresenter(this,((WeatherApp) getApplication()).getAppDataManager());

        if(getIntent() != null) {
            WeatherData data = (WeatherData) getIntent().getSerializableExtra(DATA_EXTRA);
            presenter.onSetupComplete(data);
        }
    }

    private void setFragment(Fragment fragment, FrameLayout container) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(container.getId(), fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onRefreshClicked() {
        presenter.onRefreshClick();
    }

    @Override
    public void onReady() {}

    @Override
    public void animateRefresh(boolean animate) {
        refreshFragment.setAnimating(animate);
    }

    @Override
    public void showData(WeatherData data) {
        binding.tvCity.setText(data.getName());
        binding.tvLoc.setText(Utils.formatLatLong(data.getCoord().getLat(), data.getCoord().getLon()));
        Picasso.get()
                .load(BuildConfig.WEATHER_IMAGE_HOST + data.getWeather().get(0).getIcon() + ".png")
                .placeholder(R.drawable.ic_launcher_foreground)
                .into(binding.weatherIcon);
        binding.tvTemp.setText(Utils.appendCelsius(this,Utils.twoDecimalPlaces(Utils.kelvinToCelsius(data.getMain().getTemp()))));
        binding.min.setText(getString(R.string.label_min, Utils.appendCelsius(this,Utils.twoDecimalPlaces(Utils.kelvinToCelsius(data.getMain().getTempMin())))));
        binding.max.setText(getString(R.string.label_max, Utils.appendCelsius(this,Utils.twoDecimalPlaces(Utils.kelvinToCelsius(data.getMain().getTempMax())))));
        binding.tvWeather.setText(data.getWeather().get(0).getMain());
        binding.descValue.setText(data.getWeather().get(0).getDescription());
        binding.humdidityValue.setText(data.getMain().getHumidity());
        binding.idvalue.setText(data.getId());
        binding.updateValue.setText(Utils.unixToDate(data.getDt()));
    }

    @Override
    public boolean isNetworkConnected() {
        return Utils.isNetworkConnected(getApplicationContext());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDetach();
    }
}
