package myapps.com.weatherapp.weatherdetail;

import myapps.com.weatherapp.data.model.WeatherData;

/**
 * Created by mching on 19/07/2018.
 */

public interface WeatherDetailContract {

    interface View {
        void animateRefresh(boolean animate);
        void showData(WeatherData weatherData);
        boolean isNetworkConnected();
    }

    interface Presenter {
        void onRefreshClick();
        void onSetupComplete(WeatherData data);
        void loadData();
        void onDetach();
    }
}
