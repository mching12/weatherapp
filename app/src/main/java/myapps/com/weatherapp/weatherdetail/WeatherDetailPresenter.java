package myapps.com.weatherapp.weatherdetail;

import java.util.List;

import myapps.com.weatherapp.data.AppDataManager;
import myapps.com.weatherapp.Utils;
import myapps.com.weatherapp.data.model.WeatherData;
import myapps.com.weatherapp.data.network.ApiHelper;

/**
 * Created by mching on 19/07/2018.
 */

public class WeatherDetailPresenter implements WeatherDetailContract.Presenter {

    private WeatherDetailContract.View view;
    private AppDataManager appDataManager;
    private String id;

    public WeatherDetailPresenter(WeatherDetailContract.View view, AppDataManager appDataManager) {
        this.view = view;
        this.appDataManager = appDataManager;
    }

    @Override
    public void onRefreshClick() {
        if(view == null) {
            return;
        }

        if(view.isNetworkConnected()) {
            loadData();
        }
    }

    @Override
    public void onSetupComplete(WeatherData data) {
        if(view == null) {
            return;
        }

        id = data.getId();
        view.showData(data);
    }

    @Override
    public void loadData() {
        if(view == null) {
            return;
        }

        view.animateRefresh(true);
        appDataManager.getWeatherData(new ApiHelper.ApiInteractor() {
            @Override
            public void onFinished(List<WeatherData> weatherData) {
                WeatherData data = Utils.findWeatherData(weatherData, id);
                if(data != null) {
                    view.showData(data);
                }
                view.animateRefresh(false);
                appDataManager.saveAll(Utils.convertToWeatherEntry(weatherData));
            }

            @Override
            public void onFailure(Throwable t) {
                view.animateRefresh(false);
            }
        });


    }

    @Override
    public void onDetach() {
        this.view = null;
    }
}
