package myapps.com.weatherapp.data.db;

import java.util.List;

/**
 * Created by mching on 19/07/2018.
 */

public class AppDbHelper implements DbHelper {

    private final WeatherDataDao dao;

    public AppDbHelper(WeatherDataDao dao) {
        this.dao = dao;
    }

    @Override
    public List<WeatherEntry> getAll() {
        return dao.getAll();
    }

    @Override
    public void saveAll(List<WeatherEntry> weatherEntryList) {
        dao.insertAllWeatherData(weatherEntryList);
    }
}
