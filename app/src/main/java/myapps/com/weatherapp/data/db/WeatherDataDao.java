package myapps.com.weatherapp.data.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by mching on 18/07/2018.
 */

@Dao
public interface WeatherDataDao {

    @Query("SELECT * FROM weatherentry")
    List<WeatherEntry> getAll();

    @Insert(onConflict = REPLACE)
    void insertAllWeatherData(List<WeatherEntry> weatherData);

    @Delete
    void delete(WeatherEntry weatherData);

    @Update
    void updateMovieList(List<WeatherEntry> weatherData);
}
