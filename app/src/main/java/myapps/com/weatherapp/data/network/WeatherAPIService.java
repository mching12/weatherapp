package myapps.com.weatherapp.data.network;

import myapps.com.weatherapp.data.model.WeatherDataList;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by mching on 18/07/2018.
 */

public interface WeatherAPIService {

    @Headers("Content-Type: application/json;charset=utf-8")
    @GET("group")
    Call<WeatherDataList> getWeatherByCityId(@Query("id") String cityId);
}
