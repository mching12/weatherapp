package myapps.com.weatherapp.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mching on 18/07/2018.
 */

public class WeatherDataList {

    @SerializedName("cnt")
    @Expose
    private int count;

    @SerializedName("list")
    @Expose
    private List<WeatherData> weatherData;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<WeatherData> getWeatherData() {
        return weatherData;
    }

    public void setWeatherData(List<WeatherData> weatherData) {
        this.weatherData = weatherData;
    }
}
