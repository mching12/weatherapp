package myapps.com.weatherapp.data.network;

import java.io.IOException;

import myapps.com.weatherapp.BuildConfig;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mching on 18/07/2018.
 */

public class APIServiceGenerator {

    private Retrofit retrofit;

    public APIServiceGenerator(String host, String apiKey) {
        createClient(host, apiKey);
    }

    public APIServiceGenerator(String host) {
        createClient(host, null);
    }

    private void createClient(String host, final  String apiKey) {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        //  add API Request logging when enabled
        if(BuildConfig.LOGGING_ENABLED) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(logging);
        }

        if(apiKey != null) {
            Interceptor interceptor = new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    HttpUrl originalHttpUrl = original.url();

                    HttpUrl url = originalHttpUrl.newBuilder()
                            .addQueryParameter("appid", apiKey)
                            .build();

                    // Request customization: add request headers
                    Request.Builder requestBuilder = original.newBuilder()
                            .url(url);

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            };
            httpClient.addInterceptor(interceptor);
        }

        OkHttpClient client = httpClient.build();

        retrofit = new Retrofit.Builder()
                .baseUrl(host)
                .addConverterFactory(GsonConverterFactory.create(GsonModule.getInstance().getGson()))
                .client(client)
                .build();
    }

    public <T> T createService(Class<T> service) {
        return retrofit.create(service);
    }
}
