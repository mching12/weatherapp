package myapps.com.weatherapp.data.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by mching on 18/07/2018.
 */

public class GsonModule {

    private Gson gson;
    private static Object mutex = new Object();
    private static GsonModule ourInstance = new GsonModule();

    public static GsonModule getInstance() {
        if(ourInstance == null) {
            synchronized (mutex) {
                if (ourInstance == null) ourInstance = new GsonModule();
            }
        }
        return ourInstance;
    }

    private GsonModule() {

        gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .create();
    }

    public Gson getGson() {
        return gson;
    }

}
