package myapps.com.weatherapp.data;

import java.util.List;

import myapps.com.weatherapp.data.db.DbHelper;
import myapps.com.weatherapp.data.db.WeatherEntry;
import myapps.com.weatherapp.data.network.ApiHelper;

/**
 * Created by mching on 19/07/2018.
 */

public class AppDataManager implements DbHelper, ApiHelper {

    private final DbHelper dbHelper;
    private final ApiHelper apiHelper;

    public AppDataManager(DbHelper dbHelper, ApiHelper apiHelper) {
        this.dbHelper = dbHelper;
        this.apiHelper = apiHelper;
    }

    @Override
    public List<WeatherEntry> getAll() {
        return dbHelper.getAll();
    }

    @Override
    public void saveAll(List<WeatherEntry> weatherEntryList) {
        dbHelper.saveAll(weatherEntryList);
    }

    @Override
    public void getWeatherData(ApiInteractor apiInteractor) {
        apiHelper.getWeatherData(apiInteractor);
    }
}
