package myapps.com.weatherapp.data.db;

import java.util.List;

/**
 * Created by mching on 19/07/2018.
 */

public interface DbHelper {

    List<WeatherEntry> getAll();

    void saveAll(List<WeatherEntry> weatherEntryList);
}
