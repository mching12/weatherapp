package myapps.com.weatherapp.data.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by mching on 18/07/2018.
 */

@Entity(tableName = "weatherentry")
public class WeatherEntry {

    @ColumnInfo(name="lon")
    private double lon;

    @ColumnInfo(name="lat")
    private double lat;

    @ColumnInfo(name="type")
    private String type;

    @ColumnInfo(name="sysid")
    private String sysid;

    @ColumnInfo(name="message")
    private String message;

    @ColumnInfo(name="country")
    private String country;

    @ColumnInfo(name="sunrise")
    private String sunrise;

    @ColumnInfo(name="sunset")
    private String sunset;

    @ColumnInfo(name="weatherid")
    private String weatherid;

    @ColumnInfo(name="weathermain")
    private String weathermain;

    @ColumnInfo(name="description")
    private String description;

    @ColumnInfo(name="icon")
    private String icon;

    @ColumnInfo(name="temp")
    private double temp;

    @ColumnInfo(name="pressure")
    private String pressure;

    @ColumnInfo(name="humidity")
    private String humdity;

    @ColumnInfo(name="tempmin")
    private double tempmin;

    @ColumnInfo(name="tempmax")
    private double tempmax;

    @ColumnInfo(name="visbility")
    private String visbility;

    @NonNull
    @PrimaryKey
    @ColumnInfo(name="id")
    private String id;

    @ColumnInfo(name="dt")
    private String dt;

    @ColumnInfo(name="name")
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSysid() {
        return sysid;
    }

    public void setSysid(String sysid) {
        this.sysid = sysid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getSunrise() {
        return sunrise;
    }

    public void setSunrise(String sunrise) {
        this.sunrise = sunrise;
    }

    public String getSunset() {
        return sunset;
    }

    public void setSunset(String sunset) {
        this.sunset = sunset;
    }

    public String getWeatherid() {
        return weatherid;
    }

    public void setWeatherid(String weatherid) {
        this.weatherid = weatherid;
    }

    public String getWeathermain() {
        return weathermain;
    }

    public void setWeathermain(String weathermain) {
        this.weathermain = weathermain;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public double getTemp() {
        return temp;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    public String getPressure() {
        return pressure;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public String getHumdity() {
        return humdity;
    }

    public void setHumdity(String humdity) {
        this.humdity = humdity;
    }

    public double getTempmin() {
        return tempmin;
    }

    public void setTempmin(double tempmin) {
        this.tempmin = tempmin;
    }

    public double getTempmax() {
        return tempmax;
    }

    public void setTempmax(double tempmax) {
        this.tempmax = tempmax;
    }

    public String getVisbility() {
        return visbility;
    }

    public void setVisbility(String visbility) {
        this.visbility = visbility;
    }

    public String getDt() {
        return dt;
    }

    public void setDt(String dt) {
        this.dt = dt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
