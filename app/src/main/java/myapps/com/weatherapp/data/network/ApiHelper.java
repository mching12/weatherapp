package myapps.com.weatherapp.data.network;

import java.util.List;

import myapps.com.weatherapp.data.model.WeatherData;

/**
 * Created by mching on 19/07/2018.
 */

public interface ApiHelper {

    void getWeatherData(ApiInteractor apiInteractor);

    interface ApiInteractor {
        void onFinished(List<WeatherData> weatherData);
        void onFailure(Throwable t);
    }
}
