package myapps.com.weatherapp.data.network;

import android.util.Log;

import myapps.com.weatherapp.data.model.WeatherDataList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mching on 19/07/2018.
 */

public class AppApiHelper implements ApiHelper {

    /*
     *   London Id = 2643743, Prague = 3067696, San Francisco County = 5391997
     *   3 cities are hardcoded because they are fixed requirements.
     *   Api is flexible though, can be replaced with comma separated city IDs.
     */

    private static final String TAG         = AppApiHelper.class.getSimpleName();
    private static final String CITY_IDS    = "2643743,3067696,5391997";

    private final WeatherAPIService apiService;

    public AppApiHelper(WeatherAPIService apiService) {
        this.apiService = apiService;
    }

    @Override
    public void getWeatherData(final ApiInteractor apiInteractor) {
        apiService.getWeatherByCityId(CITY_IDS).enqueue(new Callback<WeatherDataList>() {
            @Override
            public void onResponse(Call<WeatherDataList> call, Response<WeatherDataList> response) {
                if(response.isSuccessful()) {
                    apiInteractor.onFinished(response.body().getWeatherData());
                } else {
                    Log.d(TAG,"not success");
                }
            }

            @Override
            public void onFailure(Call<WeatherDataList> call, Throwable t) {
                Log.d(TAG,"fail, " + t.getMessage());
                apiInteractor.onFailure(t);
            }
        });
    }
}
