package myapps.com.weatherapp.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import myapps.com.weatherapp.R;
import myapps.com.weatherapp.Utils;
import myapps.com.weatherapp.weatherdetail.WeatherDetailActivity;
import myapps.com.weatherapp.data.model.WeatherData;

import static myapps.com.weatherapp.weatherdetail.WeatherDetailActivity.DATA_EXTRA;

/**
 * Created by mching on 18/07/2018.
 */

public class WeatherFragment extends Fragment implements View.OnClickListener {

    private static final String DATA_KEY = "data_key";

    private TextView tvCity;
    private TextView tvWeather;
    private TextView tvTemp;
    private WeatherData weatherData;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weather, container, false);
        view.setOnClickListener(this);
        this.weatherData = (WeatherData) getArguments().getSerializable(DATA_KEY);
        tvCity = view.findViewById(R.id.tv_city);
        tvCity.setText(weatherData.getName());
        tvWeather = view.findViewById(R.id.tv_weather);
        tvWeather.setText(weatherData.getWeather().get(0).getMain());
        tvTemp = view.findViewById(R.id.tv_temp);
        tvTemp.setText(Utils.appendCelsius(getActivity(),Utils.twoDecimalPlaces(Utils.kelvinToCelsius(weatherData.getMain().getTemp()))));
        return view;
    }

    public static WeatherFragment newInstance(WeatherData weatherData) {
        WeatherFragment weatherFragment = new WeatherFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(DATA_KEY, weatherData);
        weatherFragment.setArguments(bundle);
        return weatherFragment;
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(getActivity(), WeatherDetailActivity.class);
        intent.putExtra(DATA_EXTRA, weatherData);
        startActivity(intent);
    }
}
