package myapps.com.weatherapp.main;

import java.util.List;

import myapps.com.weatherapp.data.model.WeatherData;

public interface MainActivityContract {

    interface View {
        void animateRefresh(boolean animate);
        void showData(List<WeatherData> weatherDataList);
        boolean isNetworkConnected();
    }

    interface Presenter {
        void onRefreshClick();
        void onSetupComplete();
        void loadData();
        void onDetach();
    }
}
