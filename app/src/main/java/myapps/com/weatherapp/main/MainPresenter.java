package myapps.com.weatherapp.main;

import java.util.List;

import myapps.com.weatherapp.data.AppDataManager;
import myapps.com.weatherapp.Utils;
import myapps.com.weatherapp.data.model.WeatherData;
import myapps.com.weatherapp.data.network.ApiHelper;

/**
 * Created by mching on 19/07/2018.
 */

public class MainPresenter implements MainActivityContract.Presenter {

    private MainActivityContract.View view;
    private AppDataManager appDataManager;

    public MainPresenter(MainActivityContract.View view, AppDataManager appDataManager) {
        this.view = view;
        this.appDataManager = appDataManager;
    }

    @Override
    public void onRefreshClick() {
        if(view == null) {
            return;
        }

        if(view.isNetworkConnected()) {
            loadData();
        }
    }

    @Override
    public void onSetupComplete() {
        if(view == null) {
            return;
        }

        loadData();
    }

    @Override
    public void loadData() {
        if(view == null) {
            return;
        }

        if(view.isNetworkConnected()) {
            view.animateRefresh(true);
            appDataManager.getWeatherData(new ApiHelper.ApiInteractor() {
                @Override
                public void onFinished(List<WeatherData> weatherData) {
                    view.showData(weatherData);
                    view.animateRefresh(false);
                    appDataManager.saveAll(Utils.convertToWeatherEntry(weatherData));
                }

                @Override
                public void onFailure(Throwable t) {
                    view.animateRefresh(false);
                }
            });
        } else {
            view.showData(Utils.convertToWeatherData(appDataManager.getAll()));
        }
    }

    @Override
    public void onDetach() {
        this.view = null;
    }
}
