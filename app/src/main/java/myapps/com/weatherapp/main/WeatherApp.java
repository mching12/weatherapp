package myapps.com.weatherapp.main;

import android.app.Application;

import myapps.com.weatherapp.data.AppDataManager;
import myapps.com.weatherapp.BuildConfig;
import myapps.com.weatherapp.data.db.AppDatabase;
import myapps.com.weatherapp.data.db.AppDbHelper;
import myapps.com.weatherapp.data.network.APIServiceGenerator;
import myapps.com.weatherapp.data.network.AppApiHelper;
import myapps.com.weatherapp.data.network.WeatherAPIService;

/**
 * Created by mching on 19/07/2018.
 */

public class WeatherApp extends Application {

    private AppDataManager appDataManager;

    @Override
    public void onCreate() {
        super.onCreate();
        AppDbHelper appDbHelper = new AppDbHelper(AppDatabase.getAppDatabase(this).weatherDataDao());
        AppApiHelper appApiHelper = new AppApiHelper(new APIServiceGenerator(BuildConfig.WEATHER_HOST, BuildConfig.WEATHER_API_KEY).createService(WeatherAPIService.class));
        appDataManager = new AppDataManager(appDbHelper, appApiHelper);
    }

    public AppDataManager getAppDataManager() {
        return this.appDataManager;
    }
}
