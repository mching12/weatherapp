package myapps.com.weatherapp.main;

import android.databinding.DataBindingUtil;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import java.util.List;

import myapps.com.weatherapp.R;
import myapps.com.weatherapp.RefreshFragment;
import myapps.com.weatherapp.Utils;
import myapps.com.weatherapp.data.model.WeatherData;
import myapps.com.weatherapp.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements MainActivityContract.View, RefreshFragment.RefreshListener {

    /*    Notes on the project

          Implementation of UI on MainActivity SHOULD REALLY be RecyclerView + Pull to refresh.
          I implemented it this way because of the requirement to use fragments on the list and on the refresh.

          MVP used mainly on the mainscreen, other activity wasnt as important as they were pretty much static.
          Implementation SHOULD REALLY BE recyclerview with pull to refresh.
          But since fragment usage on the cities and refresh is a requirement, its built like this.

          Data is fetched from the server upon open of the app, then inserted to the DB for offline access.
          Refresh button doesn't work without a connection.

          Aesthetically speaking, UI design is admittedly terrible :))
     */

    private ActivityMainBinding binding;
    private MainActivityContract.Presenter presenter;

    private RefreshFragment refreshFrag;
    private Fragment londonFrag;
    private Fragment pragueFrag;
    private Fragment sfFrag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        initialize();
    }

    private void initialize() {
        refreshFrag = RefreshFragment.getRefreshFragment(this);
        setFragment(refreshFrag, binding.refresh);
        presenter = new MainPresenter(this, ((WeatherApp) getApplication()).getAppDataManager());
    }

    private void setFragment(Fragment fragment, FrameLayout container) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(container.getId(), fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void animateRefresh(boolean animate) {
        refreshFrag.setAnimating(animate);
    }

    @Override
    public void showData(List<WeatherData> weatherDataList) {
        londonFrag = WeatherFragment.newInstance(weatherDataList.get(0));
        pragueFrag = WeatherFragment.newInstance(weatherDataList.get(1));
        sfFrag = WeatherFragment.newInstance(weatherDataList.get(2));
        setFragment(londonFrag, binding.frame1);
        setFragment(pragueFrag, binding.frame2);
        setFragment(sfFrag, binding.frame3);
    }

    @Override
    public boolean isNetworkConnected() {
        return Utils.isNetworkConnected(getApplicationContext());
    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onRefreshClicked() {
        presenter.onRefreshClick();
    }

    @Override
    public void onReady() {
        presenter.onSetupComplete();
    }
}
