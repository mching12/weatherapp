package myapps.com.weatherapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;

import com.novoda.merlin.MerlinsBeard;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import myapps.com.weatherapp.data.db.WeatherEntry;
import myapps.com.weatherapp.data.model.Coordinates;
import myapps.com.weatherapp.data.model.Main;
import myapps.com.weatherapp.data.model.Sys;
import myapps.com.weatherapp.data.model.Weather;
import myapps.com.weatherapp.data.model.WeatherData;

/**
 * Created by mching on 18/07/2018.
 */

public class Utils {

    private static final int ANIMATE_DURATION       = 1000;
    private static final String DECIMAL_FORMAT      = "#.##";
    private static final String DATE_FORMAT         = "yyyy-MM-dd HH:mm:ss";

    public static Animation getRotateAnimation() {
        RotateAnimation rotateAnimation = new RotateAnimation(0, 360f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setInterpolator(new LinearInterpolator());
        rotateAnimation.setDuration(ANIMATE_DURATION);
        rotateAnimation.setRepeatCount(Animation.INFINITE);
        return rotateAnimation;
    }

    public static double fahrenheitToCelsius(double fahrenheit) {
        return ((fahrenheit - 32) * 5) / 9;
    }

    public static double kelvinToCelsius(double kelvin) {
        return kelvin - 273.15;
    }

    public static String appendCelsius(Context context, String temp) {
        return temp + context.getString(R.string.label_celsius);
    }

    public static String appendFahrenheit(Context context, String temp) {
        return temp + context.getString(R.string.label_fahrenheit);
    }

    public static String unixToDate(String dt) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
        return simpleDateFormat.format(new Date(Long.valueOf(dt) * 1000L));
    }

    public static String twoDecimalPlaces(double number) {
        DecimalFormat decimalFormat = new DecimalFormat(DECIMAL_FORMAT);
        return decimalFormat.format(number);
    }

    public static String formatLatLong(double lat, double lon) {
        return "(" + twoDecimalPlaces(lat) + ", " + twoDecimalPlaces(lon) + ")";
    }

    public static List<WeatherEntry> convertToWeatherEntry(List<WeatherData> weatherData) {
        List<WeatherEntry> entryList = new ArrayList<>();
        for(int i = 0; i < weatherData.size(); i++) {
            WeatherEntry weatherEntry = new WeatherEntry();
            weatherEntry.setLon(weatherData.get(i).getCoord().getLon());
            weatherEntry.setLat(weatherData.get(i).getCoord().getLat());
            weatherEntry.setType(weatherData.get(i).getSys().getType());
            weatherEntry.setSysid(weatherData.get(i).getSys().getId());
            weatherEntry.setMessage(weatherData.get(i).getSys().getMessage());
            weatherEntry.setCountry(weatherData.get(i).getSys().getCountry());
            weatherEntry.setSunrise(weatherData.get(i).getSys().getSunrise());
            weatherEntry.setSunset(weatherData.get(i).getSys().getSunset());
            weatherEntry.setWeatherid(weatherData.get(i).getWeather().get(0).getId());
            weatherEntry.setWeathermain(weatherData.get(i).getWeather().get(0).getMain());
            weatherEntry.setDescription(weatherData.get(i).getWeather().get(0).getDescription());
            weatherEntry.setIcon(weatherData.get(i).getWeather().get(0).getIcon());
            weatherEntry.setTemp(weatherData.get(i).getMain().getTemp());
            weatherEntry.setPressure(weatherData.get(i).getMain().getPressure());
            weatherEntry.setHumdity(weatherData.get(i).getMain().getHumidity());
            weatherEntry.setTempmin(weatherData.get(i).getMain().getTempMin());
            weatherEntry.setTempmax(weatherData.get(i).getMain().getTempMax());
            weatherEntry.setVisbility(weatherData.get(i).getVisibility());
            weatherEntry.setDt(weatherData.get(i).getDt());
            weatherEntry.setId(weatherData.get(i).getId());
            weatherEntry.setName(weatherData.get(i).getName());
            entryList.add(weatherEntry);
        }
        return entryList;
    }

    public static List<WeatherData> convertToWeatherData(List<WeatherEntry> weatherEntry) {
        List<WeatherData> dataList = new ArrayList<>();
        for(int i = 0; i < weatherEntry.size(); i++) {
            WeatherData weatherData = new WeatherData();
            Coordinates coordinates = new Coordinates();
            coordinates.setLat(weatherEntry.get(i).getLat());
            coordinates.setLon(weatherEntry.get(i).getLon());
            weatherData.setCoord(coordinates);
            Sys sys = new Sys();
            sys.setCountry(weatherEntry.get(i).getCountry());
            sys.setId(weatherEntry.get(i).getSysid());
            sys.setMessage(weatherEntry.get(i).getMessage());
            sys.setSunrise(weatherEntry.get(i).getSunrise());
            sys.setSunset(weatherEntry.get(i).getSunset());
            sys.setType(weatherEntry.get(i).getType());
            weatherData.setSys(sys);
            List<Weather> weatherList = new ArrayList<>();
            Weather weather = new Weather();
            weather.setDescription(weatherEntry.get(i).getDescription());
            weather.setIcon(weatherEntry.get(i).getIcon());
            weather.setId(weatherEntry.get(i).getWeatherid());
            weather.setMain(weatherEntry.get(i).getWeathermain());
            weatherList.add(weather);
            Main main = new Main();
            main.setTemp(weatherEntry.get(i).getTemp());
            main.setPressure(weatherEntry.get(i).getPressure());
            main.setHumidity(weatherEntry.get(i).getHumdity());
            main.setTempMin(weatherEntry.get(i).getTempmin());
            main.setTempMax(weatherEntry.get(i).getTempmax());
            weatherData.setMain(main);
            weatherData.setWeather(weatherList);
            weatherData.setVisibility(weatherEntry.get(i).getVisbility());
            weatherData.setDt(weatherEntry.get(i).getDt());
            weatherData.setId(weatherEntry.get(i).getId());
            weatherData.setName(weatherEntry.get(i).getName());
            dataList.add(weatherData);
        }
        return dataList;
    }

    public static boolean isNetworkConnected(Context context) {
        MerlinsBeard merlinsBeard = MerlinsBeard.from(context);
        return merlinsBeard.isConnected();
    }

    public static WeatherData findWeatherData(List<WeatherData> list, String id) {
        for(int i = 0; i < list.size(); i++) {
            if(list.get(i).getId().equalsIgnoreCase(id)) {
                return list.get(i);
            }
        }
        return null;
    }
}
