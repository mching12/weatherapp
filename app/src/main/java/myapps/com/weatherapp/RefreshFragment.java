package myapps.com.weatherapp;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by mching on 18/07/2018.
 */

public class RefreshFragment extends Fragment implements View.OnClickListener {

    private ImageView imageView;
    private TextView refreshLabel;
    private RefreshListener refreshListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_refresh, container, false);
        imageView = view.findViewById(R.id.iv_refresh);
        refreshLabel = view.findViewById(R.id.tv_refresh);
        view.setOnClickListener(this);
        return view;
    }

    private void setRefreshListener(RefreshListener refreshListener) {
        this.refreshListener = refreshListener;
    }

    public static RefreshFragment getRefreshFragment(RefreshListener listener) {
        RefreshFragment fragment = new RefreshFragment();
        fragment.setRefreshListener(listener);
        return fragment;
    }

    private void animateRefresh() {
        imageView.startAnimation(Utils.getRotateAnimation());
    }

    private void stopAnimation() {
        imageView.clearAnimation();
    }

    @Override
    public void onClick(View view) {
        if(refreshListener != null) {
            refreshListener.onRefreshClicked();
        }
    }

    public void setAnimating(boolean isAnimating) {
        if(isAnimating) {
            animateRefresh();
            refreshLabel.setText(getString(R.string.label_refreshing));
            refreshLabel.setTypeface(null, Typeface.ITALIC);
        } else {
            stopAnimation();
            refreshLabel.setTypeface(null, Typeface.BOLD);
            refreshLabel.setText(getString(R.string.label_refresh));
        }
    }

    public interface RefreshListener {
        void onRefreshClicked();
        void onReady();
    }

    @Override
    public void onStart() {
        super.onStart();
        if(refreshListener != null) {
            refreshListener.onReady();
        }
    }
}
